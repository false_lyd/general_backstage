/**
 * {
 *  title: { String|Number }
 *         显示在侧边栏、面包屑和标签栏的文字
 *  icon: (-) 该页面在左侧菜单处显示的图标，如果是自定义图标，需要在图标名称前加下划线'_'
 *  to: { String|Object }跳转链接，可传入router-link支持的任何参数
 *  target:(-) 原生a标签的target属性
 * }
 */

export default [
  {
    title: "权限设置",
    icon: "ios-key",
    auth: "auth",
    children: [
      {
        title: "管理员设置",
        to: "/admin"
      },
      {
        title: "用户角色",
        to: "/auth_group"
      },
      {
        title: "权限设置",
        to: "/auth_rule"
      }
    ]
  },
  {
    title: "商品管理",
    icon: "md-cart",
    auth: "product",
    children: [
      {
        title: "商品分类",
        auth: "category/index",
        to: "/category"
      },
      {
        title: "商品列表",
        auth: "product/index",
        to: "/product"
      },
      {
        title: "待审核商品",
        auth: "product/apply",
        to: "/product/apply_product"
      }
    ]
  },
  {
    title: "商户管理",
    icon: "md-appstore",
    auth: "store/index",
    children: [
      {
        title: "商户列表",
        to: "/store"
      }
    ]
  },
  {
    title: "会员管理",
    icon: "ios-contacts",
    auth: "auth",
    children: [
      {
        title: "会员列表",
        to: "/member"
      }
      // {
      //   title: "会员认证",
      //   to: "/member/authentication"
      // }
    ]
  },
  {
    title: "资讯管理",
    icon: "ios-information-circle",
    auth: "auth",
    children: [
      {
        title: "资讯列表",
        to: "/info"
      }
      // {
      //   title: "会员认证",
      //   to: "/member/authentication"
      // }
    ]
  },
  {
    title: "订单管理",
    icon: "logo-buffer",
    auth: "order",
    children: [
      {
        title: "订单列表",
        to: "/order"
      },
      {
        title: "售后列表",
        to: "/after_sale"
      }
    ]
  },
  {
    title: "优惠券管理",
    icon: "md-cash",
    auth: "coupons/index",
    children: [
      {
        title: "优惠券列表",
        to: "/coupons"
      }
    ]
  },
  {
    title: "轮播图管理",
    icon: "md-images",
    auth: "auth",
    to: "/banner"
  },
  {
    title: "资金管理",
    icon: "logo-yen",
    children: [
      {
        title: "店铺提现",
        auth: "store_withdraw_log/index",
        to: "/store_withdraw_log"
      }
    ]
  },
  {
    title: "系统设置",
    icon: "ios-settings",
    auth: "auth",
    to: "/setting"
  }
];
