/**
 * 项目中用到的一些写死的数据
 */

export const bulletinPublishStatus = {
  0: "未发布",
  1: "已发布"
};

export const productType = {
  1: "普通商品",
  2: "秒杀商品"
};

// 轮播图类型
export const bannerType = {
  1: "首页轮播图",
  2: "首页广告位"
};
// 轮播图跳转类型
export const bannerJumpType = {
  1: "普通商品"
};

export const couponsStatus = {
  0: "驳回",
  1: "发布",
  2: "待审核",
  3: "待提交"
};

// 订单状态
export const orderStatus = {
  0: "取消",
  1: "待付款",
  2: "待发货",
  3: "待收货",
  4: "待评价",
  5: "售后"
};

// 店铺审核状态
export const storeApplyStatus = {
  0: "驳回",
  1: "通过",
  2: "正在审核",
  3: "待审核"
};

// 售后类型
export const afterSalesType = {
  1: "仅退款",
  2: "退货退款"
};

// 售后退款货物状态
export const afterSalesGoodsStatus = {
  1: "未收到货",
  2: "已收到货"
};

// 售后状态
export const afterSalesStatus = {
  1: "申请",
  2: "同意",
  3: "不同意",
  4: "打款"
};
