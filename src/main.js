import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ViewUI from 'view-design'
// import 'view-design/dist/styles/iview.css'
import config from "@/config"
import importDirective from "@/directive"
import axios from "./libs/api.request"
import { directive as clickOutside } from "v-click-outside-x"
import "./index.less";

Vue.prototype.path = process.env.VUE_APP_SERVER_DOMAIN;

Vue.component("singleImage", () => import("./components/singleImage"));
Vue.use(ViewUI);

Vue.config.productionTip = false

/**
 * @description 生产环境关掉提示
 */
Vue.config.productionTip = false;
/**
 * @description 全局注册应用配置
 */
Vue.prototype.$config = config;
/**
 * 注册指令
 */
importDirective(Vue);
Vue.directive("clickOutside", clickOutside);

Vue.prototype.$axios = axios;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
